<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients_base', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('email', 255);
            $table->timestamps();
        });

        Schema::create('clients', function (Blueprint $table) {
            $table->uuid('client_id');
            $table->string('passport_num', 255);
        });

        Schema::create('clients_eng', function (Blueprint $table) {
            $table->uuid('client_id');
            $table->string('snn', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tables');
    }
}
