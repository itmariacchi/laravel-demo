<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Тип сущности (Клиент, заказ...)
 * @link
 */
class EntityTypeEnum extends Enum
{
    public const CLIENT = 'client';
    public const AGREEMENT = 'agreement';
}
