<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Ресурс - информация о клиенте
 * @link
 */
class Client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $this \App\Domain\Client\Model\ClientBase
         */
        return [
            'id' => $this->id,
            'email' => $this->email,
        ];
    }
}
