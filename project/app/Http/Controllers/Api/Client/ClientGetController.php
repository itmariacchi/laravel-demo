<?php

namespace App\Http\Controllers\Api\Client;

use App\Domain\Client\Model\Client;
use App\Domain\Client\Model\ClientBase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

/**
 * Контроллер для получения данных клиента
 * @link
 */
class ClientGetController extends BaseController
{
    public function __invoke(Request $request, ClientBase $client)
    {
        // TODO::implement, реализовать через ресурс
        // TODO::решить проблему унифицированного ответа, т.е. [success => true, data => ....], сделать метод в базовом контроллере
        return response()->json([
            'data' => [

            ],
        ]);
    }
}
