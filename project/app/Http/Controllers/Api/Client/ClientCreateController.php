<?php

namespace App\Http\Controllers\Api\Client;

use App\Domain\Client\UseCase\Create\ClientCreateCommand;
use App\Infrastructure\Command\CommandDispatcher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Support\Str;

/**
 * Контроллер для создания профиля клиента
 * @link
 */
class ClientCreateController extends BaseController
{
    public function create(Request $request, CommandDispatcher $commandDispatcher)
    {
        try {
            // TODO::skip validation
            $command = ClientCreateCommand::fromArray([
                // TODO::решить проблему с цифровыми ид, они могут быть нужны. Например создали договор, клиенту нужно показать номер
                // TODO::анализ быстродействия
                'uuid' => Str::uuid()->toString(),
                'email' => $request->get('email'),
                'passport_num' => $request->get('passport_num'),
            ]);
        } catch (\Exception $e) {
            //TODO::do something with validation exception (not 404), сделать единый формат ответа (тип запроса указать)
            dd($e);

        }

        $commandDispatcher->processCommand($command);

        return response()->json([
            'data' => [
                'user_id' => $command->getUuid(),
            ],
        ]);
    }
}
