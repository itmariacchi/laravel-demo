<?php

namespace App\Http\Controllers\Api\Client\Eng;

use App\Domain\Client\UseCase\Create\ClientCreateCommand;
use App\Domain\Client\UseCase\Eng\Create\ClientEngCreateCommand;
use App\Domain\Client\UseCase\Eng\Create\ClientEngCreateCommandHandler;
use App\Infrastructure\Command\CommandDispatcher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Support\Str;

/**
 * Контроллер для создания профиля клиента
 * @link
 */
class ClientEngCreateController extends BaseController
{
    // TODO::роут один, язык в заголовке. В одном контроллере разные методы invokeRU, invokeEN
    public function __invoke(Request $request, CommandDispatcher $commandDispatcher)
    {
        try {
            $command = ClientEngCreateCommand::fromArray([
                'uuid' => Str::uuid()->toString(),
                'snn' => $request->get('snn'),
            ]);
        } catch (\Exception $e) {
            //TODO::do something with validation exception (not 404)
            dump($e);
            die;
        }

        $commandDispatcher->processCommand($command);

        return response()->json([
            'data' => [
                'user_id' => $command->getUuid(),
            ],
        ]);
    }
}
