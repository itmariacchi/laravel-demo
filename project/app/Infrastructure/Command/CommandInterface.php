<?php

namespace App\Infrastructure\Command;

use App\Enums\EntityTypeEnum;

/**
 * TODO::LoggableInterface
 * Интерфейс для всех команд
 * @link
 */
interface CommandInterface
{
    /**
     * Возвращает краткий заголовок команды, например "Регистрация пользователя", "Изменение статуса заказа"
     * @return string
     */
    public static function getTitle(): string;

    /**
     * Возвращает уникальный код команды, например client.activate, order.change-status
     * @return string
     */
    public static function getCode(): string;

    /**
     * Возвращает тип обрабатываемой сущности (Клиент, заказ, договор...)
     * @return EntityTypeEnum
     */
    public static function getEntityType(): EntityTypeEnum;

    /**
     * Возвращает идентификатор обрабатываемой сущности (ид заказа, ид договора...)
     * @return string
     */
    public function getEntityId(): string;
}
