<?php

namespace App\Infrastructure\Command;

use App\Exceptions\CommandException;
use App\Infrastructure\Logger\CommandLogger;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Диспетчер команд, определяет обработчик для команды, выполняет команду
 * Перехватывает исключения и бросает CommandException, которое содержит доп. отладочную информацию
 * @link
 */
class CommandDispatcher
{
    /**
     * @var CommandLogger
     */
    private $commandLogger;

    public function __construct(CommandLogger $commandLogger)
    {
        $this->commandLogger = $commandLogger;
    }

    /**
     * @param CommandInterface $command
     * @param bool $async
     * @throws CommandException
     */
    public function processCommand(CommandInterface $command, bool $async = false): void
    {
        try {
            // TODO::валидатор лежит не в одном месте
            // TODO::отправка в монолог, сентри
            $commandHandlerClass = $this->getCommandHandlerClass($command);
            if ($async) {
                $commandHandlerClass::dispatch($command);
            } else {
                $commandHandlerClass::dispatchNow($command);
            }

            $this->commandLogger->logCommandFinished($command);
        } catch (\Throwable $ex) {
            throw new CommandException($command, $ex);
        }
    }

    /**
     * Получает имя класса обработчика команды
     * Алгоритм простейший, добавляется суффикс Handler к классу команды
     * @param CommandInterface $command
     * @return Dispatchable
     * @throws \RuntimeException
     */
    private function getCommandHandlerClass(CommandInterface $command): string
    {
        $commandHandlerClass = get_class($command) . 'Handler';

        if (!class_exists($commandHandlerClass)) {
            throw new \RuntimeException(
                sprintf(
                    'Невозможно выполнить команду %s, класс-обработчик %s не найден',
                    get_class($command),
                    $commandHandlerClass
                )
            );
        }

        return $commandHandlerClass;
    }
}
