<?php

namespace App\Infrastructure\Model;

use Illuminate\Support\Str;

/**
 * Трейт, позволяющий использовать UUID в качестве ключа сущности
 * @link
 */
trait UsesUuidTrait
{
    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }
}
