<?php

namespace App\Infrastructure\Logger;

use App\Infrastructure\Command\CommandInterface;

/**
 * Логер для команд, содержит удобные методы для логирования из команд
 * @link
 */
class CommandLogger
{
    /**
     * Логирует ошибку в команде
     * @param CommandInterface $command
     * @param \Throwable $ex
     */
    public function logException(CommandInterface $command, \Throwable $ex): void
    {
        $message = sprintf('Ошибка в команде %s: %s', $command::getTitle(), $ex->getMessage());
        \Log::error($message, [
            'command' => $command::getCode(),
            'entity' => $command::getEntityType()->value,
            'entity_id' => $command->getEntityId(),
            'trace' => $this->getTraceWithoutVendor($ex),
        ]);
    }

    /**
     * Логирует информацию о работе команды, например "Пользователь создан", "Договор переведен в статус Активный"
     * @param CommandInterface $command
     * @param string $message
     */
    public function logInfo(CommandInterface $command, string $message): void
    {
        \Log::info($message, [
            'command' => $command::getCode(),
            'entity' => $command::getEntityType()->value,
            'entity_id' => $command->getEntityId(),
        ]);
    }

    /**
     * Логирует факт выполнения команды
     * @param CommandInterface $command
     */
    public function logCommandFinished(CommandInterface $command): void
    {
        $this->logInfo($command, sprintf('Команда "%s" выполнена', $command::getTitle()));
    }

    /**
     * Получает трейс исключения без папок vendor
     * @param \Throwable $ex
     * @return string
     */
    private function getTraceWithoutVendor(\Throwable $ex): string
    {
        $lines = explode('#', $ex->getTraceAsString());
        $lines = array_filter($lines, function ($line) {
            return !empty($line) && strpos($line, '\\vendor\\') === false;
        });

        return implode('#', $lines);
    }
}
