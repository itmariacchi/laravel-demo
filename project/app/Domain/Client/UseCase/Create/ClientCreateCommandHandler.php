<?php

namespace App\Domain\Client\UseCase\Create;

use App\Domain\Client\Events\ClientRegisteredEvent;
use App\Domain\Client\Mail\ClientRegisteredMail;
use App\Domain\Client\Model\Client;
use App\Domain\Client\Model\ClientBase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;

/**
 * Обработчик команды создания нового клиента
 * @link
 */
class ClientCreateCommandHandler implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;

    /**
     * @var ClientCreateCommand
     */
    private $command;

    public function __construct(ClientCreateCommand $command)
    {
        $this->command = $command;
    }

    public function handle(): void
    {
        $this->createClient($this->command);

        $this->sendClientRegisteredEmail($this->command->getEmail());
    }

    /**
     * Сохраняет клиента в БД
     * @param ClientCreateCommand $command
     */
    private function createClient(ClientCreateCommand $command): void
    {
        $client = new Client();

        $client->id = $this->command->getUuid();
        $client->email = $this->command->getEmail();

        $client->save();

        //TODO::save related table data
        //$client->info->save(['passport_num' => $this->command->getPassportNum()]);

        event(new ClientRegisteredEvent($client));
    }

    /*
     * Отправляет email с регистрационными данными
     */
    private function sendClientRegisteredEmail(string $clientEmail): void
    {
        \Mail::to($clientEmail)->queue(new ClientRegisteredMail());
    }
}
