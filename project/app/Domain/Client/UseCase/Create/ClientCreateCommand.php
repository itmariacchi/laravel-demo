<?php

namespace App\Domain\Client\UseCase\Create;

use App\Enums\EntityTypeEnum;
use App\Infrastructure\Command\CommandInterface;
use Validator;

/**
 * Команда создания нового клиента
 * @link
 */
class ClientCreateCommand implements CommandInterface
{
    /**
     * @var string
     */
    private $passportNum;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $uuid;

    public static function getTitle(): string
    {
        return 'Создание нового клиента';
    }

    public static function getCode(): string
    {
        return 'client.create';
    }

    public static function getEntityType(): EntityTypeEnum
    {
        return new EntityTypeEnum(EntityTypeEnum::CLIENT);
    }

    public function getEntityId(): string
    {
        return $this->uuid;
    }

    public static function fromArray(array $data): self
    {
        $validator = Validator::make(
            $data,
            [
                'email' => 'required | email',
                'uuid' => 'required | uuid',
                'passport_num' => 'regex:/^\d{4}$/',
            ]
        );

        $validator->validate();

        $command = new self();

        $command->uuid = $data['uuid'];
        $command->passportNum = $data['passport_num'];
        $command->email = $data['email'];

        return $command;
    }

    public function getPassportNum(): string
    {
        return $this->passportNum;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
