<?php

namespace App\Domain\Client\UseCase\Eng\Create;

use App\Domain\Client\Model\ClientEng;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;

/**
 * Обработчик команды создания нового клиента
 * @link
 */
class ClientEngCreateCommandHandler implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;

    /**
     * @var ClientEngCreateCommand
     */
    private $command;

    public function __construct(ClientEngCreateCommand $command)
    {
        $this->command = $command;
    }

    public function handle()
    {
        $client = new ClientEng();

        $client->client_id = $this->command->getUuid();
        $client->snn = $this->command->getSnn();

        $client->save();
    }
}
