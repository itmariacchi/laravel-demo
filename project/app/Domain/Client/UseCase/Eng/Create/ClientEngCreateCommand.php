<?php

namespace App\Domain\Client\UseCase\Eng\Create;

use App\Enums\EntityTypeEnum;
use App\Infrastructure\Command\CommandInterface;

/**
 * Команда создания нового клиента
 * @link
 */
class ClientEngCreateCommand implements CommandInterface
{
    /**
     * @var string
     */
    private $passportNum;

    /**
     * @var string
     */
    private $snn;

    public static function getTitle(): string
    {
        return 'Создание нового клиента (ENG)';
    }

    public static function getCode(): string
    {
        return 'client.create';
    }

    public static function getEntityType(): EntityTypeEnum
    {
        return new EntityTypeEnum(EntityTypeEnum::CLIENT);
    }

    public static function fromArray(array $data): self
    {
        //TODO::implement
        return new self();
    }

    public function getEntityId(): string
    {
        // TODO: Implement getEntityId() method.
    }
}
