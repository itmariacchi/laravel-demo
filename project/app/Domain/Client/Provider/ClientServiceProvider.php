<?php

namespace App\Domain\Client\Provider;

use Illuminate\Support\ServiceProvider;

/**
 * @link
 */
class ClientServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../config/routes.php');
    }
}
