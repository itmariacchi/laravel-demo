<?php

namespace App\Domain\Client\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Уведомление о регистрации нового клиента, отправляется клиенту
 */
class ClientRegisteredMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    public function __construct()
    {
    }

    public function build(): ClientRegisteredMail
    {
        return $this->from('opensansara@yandex.ru')->view('email.client_registered');
    }
}
