<?php

namespace App\Domain\Client\Service;

/**
 * Сервис уведомлений клиента
 * @link
 */
class ClientNotificationService
{
    public function sendClientGreetingEmail(string $email): void
    {
        // TODO::продумать общий сервис уведомлений (смс, email, telegram...)
        // чтобы управление было в одном месте, это поможет избежать путаницы
        // т.к. часто встречается задача, разобраться, кому и когда мы шлем уведомления
        // продумать хранение всех уведомлений в БД (за последний год), чтобы всегда можно было узнать, что мы отправили
        // Образец https://github.com/shvetsgroup/laravel-email-database-log/blob/master/src/ShvetsGroup/LaravelEmailDatabaseLog/EmailLogger.php
        // Для смс кажется можно использовать встроенный механизм, но лучше написать свой, чтобы все уведомления хранились в одной таблице
        // send email
    }
}
