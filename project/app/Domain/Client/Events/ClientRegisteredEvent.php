<?php

namespace App\Domain\Client\Events;

use App\Domain\Client\Model\Client;

/**
 * Событие регистрации нового клиента
 * @link
 */
class ClientRegisteredEvent
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getClient(): Client
    {
        return $this->client;
    }
}
