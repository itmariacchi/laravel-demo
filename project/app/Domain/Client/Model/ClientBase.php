<?php

namespace App\Domain\Client\Model;

use App\Infrastructure\Model\UsesUuidTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Базовая модель Клиент
 * Содержит общие поля для всех клиентов, регионо-зависимые данные располагаются в моделях-наследниках
 * @link
 */
class ClientBase extends Model
{
    use UsesUuidTrait;

    protected $table = 'clients_base';

    protected $primaryKey = 'id';

    // TODO::обсудить и реализовать наследование (нормализованные таблицы, json. Как организовать модели.)
    protected $fillable = [
        'id',
        'email',
    ];
}
