<?php

namespace App\Domain\Client\Model;

/**
 * Модель клиент
 * @link
 */
class ClientEng extends ClientBase
{
    protected $table = 'clients_eng';

    protected $fillable = [
        'client_id',
        'snn',
    ];

    public function getSnnAttribute(): string
    {
        return $this->snn;
    }
}
