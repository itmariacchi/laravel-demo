<?php

namespace App\Domain\Client\Model;

use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Модель клиент
 * @link
 */
class Client extends ClientBase
{
    public function info(): HasOne
    {
        // TODO::laravel multitenance
        // TODO::laravel scout
        /*
         * clients_base
         *  id
         *  name
         *
         * clients_ru
         *  passport
         *
         * clients_en
         *  snn
         *
         * clients
         *  id
         *  name
         *  json
         */

        return $this->hasOne(ClientData::class, 'client_id', 'id');
    }

    public function getPassportNumAttribute(): string
    {
        return $this->info->passport_num;
    }
}
