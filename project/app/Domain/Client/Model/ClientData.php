<?php

namespace App\Domain\Client\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Доп. данные обычного клиента
 * Обычный, т.е. клиент по-умолчанию для всех стран
 * В случае различия полей клиента для других стран будут созданы отдельные модели
 * @link
 */
class ClientData extends Model
{
    protected $table = 'clients';

    protected $fillable = [
        'client_id',
        'passport_num',
    ];
}
