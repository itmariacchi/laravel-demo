<?php
/**
 * @link
 */

use Illuminate\Support\Facades\Route;

Route::prefix('api')->group(static function () {
    //TODO::сделать базовый контроллер
    Route::get('/client/create/', [\App\Http\Controllers\Api\Client\ClientCreateController::class, 'create']);
    Route::get('/client/{client}/', [\App\Http\Controllers\Api\Client\ClientGetController::class, 'index']);
    Route::get('/client/eng/create/', \App\Http\Controllers\Api\Client\Eng\ClientEngCreateController::class);
});
