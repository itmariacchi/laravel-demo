<?php

namespace App\Domain\Agreement\Provider;

use App\Domain\Agreement\Listeners\ActivateAgreementWhenClientRegisteredListener;
use App\Domain\Client\Events\ClientRegisteredEvent;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * @link
 */
class AgreementServiceProvider extends ServiceProvider
{
    protected $listen = [
        ClientRegisteredEvent::class => [
            ActivateAgreementWhenClientRegisteredListener::class,
        ],
    ];
}
