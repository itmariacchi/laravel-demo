<?php

namespace App\Domain\Agreement\Listeners;

use App\Domain\Client\Events\ClientRegisteredEvent;

/**
 * Активация договора после регистрации клиента
 * @link
 */
class ActivateAgreementWhenClientRegisteredListener
{
    public function handle(ClientRegisteredEvent $event)
    {
        \Log::debug('client registered event handler: ' . $event->getClient()->id, [
            'func_name' => __METHOD__,
            'client_id' => $event->getClient()->id,
        ]);
    }
}
