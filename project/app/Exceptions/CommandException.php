<?php

namespace App\Exceptions;

use App\Infrastructure\Command\CommandInterface;
use Throwable;

/**
 * Исключение, произошедшее во время выполнения команды
 * Содержит дополнительную отладочную информацию
 * @link
 */
class CommandException extends \Exception
{
    /**
     * @var CommandInterface
     */
    private $command;

    /**
     * @var \Throwable
     */
    private $exception;

    public function __construct(CommandInterface $command, \Throwable $ex)
    {
        $this->command = $command;
        $this->exception = $ex;

        parent::__construct($ex->getMessage(), is_numeric($ex->getCode()) ? $ex->getCode() : 0, $ex);
    }

    public function getCommand(): CommandInterface
    {
        return $this->command;
    }

    public function getException(): Throwable
    {
        return $this->exception;
    }
}
