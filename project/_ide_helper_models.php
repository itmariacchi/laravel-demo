<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Domain\Client\Model{
/**
 * Модель клиент
 *
 * @link
 * @property string $id
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Domain\Client\Model\ClientData|null $data
 * @property-read string $passport_num
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\Client query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\Client whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\Client whereUpdatedAt($value)
 */
	class Client extends \Eloquent {}
}

namespace App\Domain\Client\Model{
/**
 * Базовая модель Клиент
 * Содержит общие поля для всех клиентов, регионо-зависимые данные располагаются в моделях-наследниках
 *
 * @link
 * @property string $id
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientBase newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientBase newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientBase query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientBase whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientBase whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientBase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientBase whereUpdatedAt($value)
 */
	class ClientBase extends \Eloquent {}
}

namespace App\Domain\Client\Model{
/**
 * App\Domain\Client\Model\ClientData
 *
 * @link 
 * @property string $client_id
 * @property string $passport_num
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientData query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientData whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientData wherePassportNum($value)
 */
	class ClientData extends \Eloquent {}
}

namespace App\Domain\Client\Model{
/**
 * Модель клиент
 *
 * @link
 * @property string $client_id
 * @property string $snn
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientEng newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientEng newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientEng query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientEng whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\Client\Model\ClientEng whereSnn($value)
 */
	class ClientEng extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 */
	class User extends \Eloquent {}
}

